<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{

    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('phone',255);
            $table->string('address',255);
            $table->string('photo',255);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
