<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schools extends Model
{
    protected $fillable = ['name','phone','address','photo'];
}
