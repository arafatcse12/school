<?php

namespace App\Http\Controllers;

use App\schools;
use Illuminate\Http\Request;
use Auth;
use Image;
use Storage;

class StudentController extends Controller
{

    public function index()
    {
       $schools=schools::all();
        return view('/admin/school/index',compact('schools'));
    }


    public function create()
    {

    }


    public function store(Request $request)
    {
        $photo="";

        if($request->hasFile('photo'))
        {
            $destinationPath="photo";
            $file=$request->photo;
           $extention=$file->getClientOriginalExtension();
           $filename=rand(1111,9999).".".$extention;
           $file->move($destinationPath,$filename);
            $photo=$filename;

        }
      $data=['name'=>$request->name,
          'phone'=>$request->phone,
          'address'=>$request->address,
          'photo'=>$photo];



       $schools= schools::create($data);
        return redirect('/admin/school/index');
    }


    public function show($id)
    {
        $schools= schools::find($id);
        return view('/admin/school/show',compact('schools'));
    }


    public function edit($id)
    {
        $schools= schools::find($id);
        return view('/admin/school/edit',compact('schools'));
    }


    public function update(Request $request, $id)
    {
        $photo="";

        if($request->hasFile('photo'))
        {
            $destinationPath="photo";
            $file=$request->photo;
            $extention=$file->getClientOriginalExtension();
            $filename=rand(1111,9999).".".$extention;
            $file->move($destinationPath,$filename);
            $oldFilname=$photo;

            $photo=$filename;
            Storage::delete($oldFilname);
            $filename=($photo);

        }
        $data=['name'=>$request->name,
            'phone'=>$request->phone,
            'address'=>$request->address,
            'photo'=>$photo];
        $schools=schools::find($data);
        $schools->update($data);
        return redirect('/admin/school/index');

    }

    public function destroy($id)
    {
        schools::destroy($id);
        return redirect('/admin/school/index');
    }




}
