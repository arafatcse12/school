<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/school/', function () {
    return view('admin.school.index');
});

Route::get('/admin/school/create', function () {
    return view('admin.school.create');
});
Route::get('/admin/school/store', function () {
    return view('admin.school.create');
});
Route::post('/admin/school/store', 'StudentController@store');
Route::get('/admin/school/index', 'StudentController@index');
Route::get('/admin/school/show/{id}', 'StudentController@show');
Route::get('/admin/school/edit/{id}', 'StudentController@edit');
Route::patch('/admin/school/update/{id}', 'StudentController@update');
Route::get ('/admin/school/destroy/{id}', 'StudentController@destroy');


Route::get('/admin/upload/create', function () {
    return view('admin.upload.create');
});

Route::post ('/admin/upload/store/', 'UploadController@store');
Route::get ('/admin/upload/index/', 'UploadController@index');


