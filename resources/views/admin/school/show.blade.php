@extends('admin.layouts.master')
@section('show')

<table class="table table-bordered">
    <tbody>

    <h4>Information Data Table</h4><tr>

        <th>id</th>
        <th>name</th>
        <th>phone</th>
        <th>address</th>
        <th>photo</th>

    </tr>

        <tr>

            <td>{{$schools->id}}</td>
            <td>{{$schools->name}}</td>
            <td>{{$schools->phone}}</td>
            <td>{{$schools->address}}</td>
            <td>{{Html::image('photo/'.$schools->photo,'photo',['style'=>'width:90px;height:90px'])}}</td>


        </tr>

    </tbody></table>

    @endsection