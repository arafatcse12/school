@extends('admin.layouts.master')
@section('view')
    <div class="col-md-6">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Bordered Table</h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>

                    <th>id</th>
                    <th>name</th>
                    <th>phone</th>
                    <th>address</th>
                    <th>photo</th>
                    <th>Action</th>
                </tr>
                @foreach($schools as $schools)
                <tr>


                    <td>{{$schools->id}}</td>
                    <td>{{$schools->name}}</td>
                    <td>{{$schools->phone}}</td>
                    <td>{{$schools->address}}</td>
                    <td>{{Html::image('photo/'.$schools->photo,'photo',['style'=>'width:90px;height:90px'])}}</td>

                    <th>
                        <a  href="{{url('admin/school/show/'.$schools->id)}}">Show</a>
                        <a  href="{{url('admin/school/edit/'.$schools->id)}}">Edit</a>
                        <a href="{{url('admin/school/destroy/'.$schools->id)}}">Delete</a>
                    </th>

                </tr>
                @endforeach
                </tbody></table>
        </div>


    </div>

    </div>


    @endsection