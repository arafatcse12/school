@extends('admin.layouts.master')
@section('from')

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Student registration form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['url' => '/admin/school/store/','enctype'=>'multipart/form-data','method' => 'post']) !!}
        {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputName1">Student Name</label>
                    <input type="text" class="form-control" id="exampleInputName1" name="name" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputPhone1">Phone</label>
                    <input type="text" class="form-control" id="exampleInputPhone1"  name="phone"  placeholder="Phone">
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" rows="3"  name="address"  placeholder="Your Address"></textarea>
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Upload Image</label>
                    <input type="file" name="photo" >
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="exampleInputFile">Image</label>--}}
                    {{--<input type="file" id="exampleInputFile">--}}
                {{--</div>--}}

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        {{ Form::close() }}
    </div>
            </div>
        </div>

    </section>

@endsection





