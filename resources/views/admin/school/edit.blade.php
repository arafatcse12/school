@extends('admin.layouts.master')
@section('from')

    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Student registration form</h3>
                    </div>

                    {!! Form::open(['url' => '/admin/school/update/'.$schools->id,'method' => 'patch']) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('name')}}
                            {{Form::text('name',$schools->name,['class'=>'form-control'])}}
                            {{--<input type="text" class="form-control" id="exampleInputName1" {{$schools->name}} placeholder="Your Name">--}}
                        </div>
                        <div class="form-group">
                            {{Form::label('phone')}}
                            {{Form::text('phone',$schools->phone,['class'=>'form-control'])}}

                            {{--<label for="exampleInputPhone1">Phone</label>--}}
                            {{--<input type="text" class="form-control" id="exampleInputPhone1" {{$schools->phone}} name="phone"  placeholder="Phone">--}}
                        </div>

                        <div class="form-group">

                            {{Form::label('address')}}
                            {{Form::text('address',$schools->address,['class'=>'form-control'])}}

                            {{--<label>Address</label>--}}
                            {{--<textarea class="form-control" rows="3"  name="address" {{$schools->address}} placeholder="Your Address"></textarea>--}}
                        </div>
                        <div class="form-group">
                            {{Form::label('photo')}}
                            {{Html::image('photo/'.$schools->photo,'photo',['style'=>'width:90px;height:90px'])}}<br>
                            <label for="exampleInputName1">Upload Image</label>
                            <input type="file" name="photo" >
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">

                        </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputName1">Upload Image</label>--}}
                                {{--<input type="file" name="photo" >--}}
                                {{--<input type="hidden" value="{{ csrf_token() }}" name="_token">--}}
                            {{--</div>--}}
                            {{--{{Form::label('photo')}}--}}
                            {{--{{Html::image('photo/'.$schools->photo,'photo',['style'=>'width:90px;height:90px'])}}--}}


                            {{--<label>Address</label>--}}
                            {{--<textarea class="form-control" rows="3"  name="address" {{$schools->address}} placeholder="Your Address"></textarea>--}}
                        </div>


                        {{--<div class="form-group">--}}
                        {{--<label for="exampleInputFile">Image</label>--}}
                        {{--<input type="file" id="exampleInputFile">--}}
                        {{--</div>--}}

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </section>

@endsection


